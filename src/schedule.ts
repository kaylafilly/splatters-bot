import { Guild } from 'discord.js'

export default function (guild: Guild) {
  setInterval(async () => {
    const now = new Date(new Date().toLocaleString('en-US', { timeZone: 'America/Winnipeg' }))
    if (now.getMinutes() !== 0) return

    try {
      switch (now.getHours()) {
        case 0:
          await guild.setIcon('./src/icons/iconlatenight.png')
          await guild.setBanner('./src/icons/backgroundlatenite.png')
          break;
        case 5:
          await guild.setIcon('./src/icons/icondawn.png')
          await guild.setBanner('./src/icons/backgrounddawn.png')
          break;
        case 8:
          await guild.setIcon('./src/icons/iconmorning2.png')
          await guild.setBanner('./src/icons/morningbackground.png')
          break;
        case 12:
          await guild.setIcon('./src/icons/icondayanim.gif')
          await guild.setBanner('./src/icons/backgroundday.png')
          break;
        case 15:
          await guild.setIcon('./src/icons/afternoonicon1.gif')
          await guild.setBanner('./src/icons/backgroundafternoon.png')
          break;
        case 18:
          await guild.setIcon('./src/icons/eveningicon.gif')
          await guild.setBanner('./src/icons/backgroundevening.png')
          break;
        case 21:
          await guild.setIcon('./src/icons/iconnight.png')
          await guild.setBanner('./src/icons/nightbackground.png')
          break;
      }
    } catch (err) {
      console.error(err)
    }
  }, 60000)
}