import * as dotenv from 'dotenv'
dotenv.config()

import { Client, Events, GatewayIntentBits } from 'discord.js'
import schedule from './schedule'
import './commands'

const client = new Client({ intents: [GatewayIntentBits.Guilds] })

client.once(Events.ClientReady, async c => {
  console.log(`Ready! Logged in as ${c.user.tag}`)

  const guild = await c.guilds.fetch(`${process.env.GUILD}`)

  // Start icon changing schedule
  schedule(guild)
})

// client.on('interactionCreate', async interaction => {
//   if (!interaction.isChatInputCommand()) return;

//   if (interaction.commandName === 'ping') {
//     await interaction.reply('Pong!');
//   }
// });

client.login(`${process.env.TOKEN}`)