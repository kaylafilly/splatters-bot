# Splatbot

Mini discord bot for Splatters.

## Getting started

    npm install
    npm start

with docker:

    docker run -e "TOKEN=bot token" -e "CLIENT_ID=bot client id" -e "GUILD=guild snoflake" registry.gitlab.com/kaylafilly/splatters-bot:latest