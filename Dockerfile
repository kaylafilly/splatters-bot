# syntax=docker/dockerfile:1
FROM node:19-alpine

COPY . /app

WORKDIR /app

RUN apk add --update --no-cache python3 build-base gcc && ln -sf /usr/bin/python3 /usr/bin/python

RUN npm install && mv node_modules ../

CMD npm run start
